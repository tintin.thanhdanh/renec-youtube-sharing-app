import { Controller } from '@nestjs/common';
import { AuthService } from './auth.service';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { RegistationInput } from './inputs/registration.input';
import { LoginInput } from './inputs/login.input';
import { GetUserInput } from './inputs/get-user.input';
@Controller()
export class AuthController {
  constructor(private readonly authService: AuthService) {}
  @MessagePattern('register')
  async register(@Payload() data: RegistationInput) {
    return await this.authService.register(data);
  }
  @MessagePattern('login')
  async login(@Payload() data: LoginInput) {
    return await this.authService.login(data);
  }
  @MessagePattern('get-user')
  async getUser(@Payload() data: GetUserInput) {
    return await this.authService.getUser(data._id);
  }
}
