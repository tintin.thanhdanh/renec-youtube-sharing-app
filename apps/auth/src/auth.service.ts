import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import { ConflictException, Injectable } from '@nestjs/common';
import { RegistationInput } from './inputs/registration.input';
import { LoginInput } from './inputs/login.input';
import { User, UserDocument } from '@app/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { BaseService } from '@app/common/utils/base-service';
import { AuthorizationType } from '@app/common/type/authorization.type';

@Injectable()
export class AuthService extends BaseService {
  constructor(
    @InjectModel(User.name, 'users') private UserModel: Model<UserDocument>,
  ) {
    super(UserModel);
  }
  async register(
    registrationInput: RegistationInput,
  ): Promise<AuthorizationType> {
    try {
      const hashedPassword = await this.hashPassword(
        registrationInput.password,
      );
      const user = await this.createOne({
        ...registrationInput,
        password: hashedPassword,
      });
      const token = this.createToken(user);
      return {
        access_token: token,
      };
    } catch (error) {
      throw new ConflictException(error.message);
    }
  }
  async login(loginInput: LoginInput): Promise<AuthorizationType> {
    const user = await this.findOne({
      email: loginInput.email,
    });
    if (!user) throw new ConflictException('User does not exist');
    const isPasswordValid = await this.comparePasswords(
      loginInput.password,
      user.password,
    );
    if (!isPasswordValid) throw new ConflictException('Invalid credentials');
    const token = this.createToken(user);
    return {
      access_token: token,
    };
  }
  async getUser(id: string): Promise<User> {
    return await this.findById(id);
  }
  private async hashPassword(password: string): Promise<string> {
    return await bcrypt.hash(password, 10);
  }
  private async comparePasswords(
    password: string,
    hashPassword: string,
  ): Promise<boolean> {
    return await bcrypt.compare(password, hashPassword);
  }
  private createToken(user: User): string {
    const expiresIn = process.env.JWT_EXPIRES_IN;
    const secret = process.env.JWT_SECRET;
    const dataStoredInToken = {
      _id: user._id,
      name: user.name,
    };
    return jwt.sign(dataStoredInToken, secret, { expiresIn });
  }
}
