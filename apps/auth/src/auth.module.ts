import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { User, UserSchema } from '@app/common';
import { ConfigModule } from '@nestjs/config';
import { AuthController } from './auth.controller';
import { MongooseModule } from '@nestjs/mongoose';
@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    MongooseModule.forRoot(process.env.DATABASE_URL, {
      connectionName: 'users',
    }),
    MongooseModule.forFeature(
      [
        {
          name: User.name,
          schema: UserSchema,
          collection: 'users',
        },
      ],
      'users',
    ),
  ],
  providers: [AuthService],
  controllers: [AuthController],
})
export class AuthModule {}
