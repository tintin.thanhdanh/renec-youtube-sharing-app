import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from './auth/auth.module';
import { ShareModule } from './share/share.module';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { GraphQLUserInterceptor } from '@app/common/interceptor/user.interceptor';
import { join } from 'path';
@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    GraphQLModule.forRoot<ApolloDriverConfig>({
      autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
      sortSchema: true,
      driver: ApolloDriver,
      // resolvers: { JSON: GraphQLJSON },
      // debug: false,
      // playground: false,
    }),
    AuthModule,
    ShareModule,
  ],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: GraphQLUserInterceptor,
    },
  ],
  controllers: [],
})
export class AppModule {}
