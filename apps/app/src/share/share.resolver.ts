import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { ShareService } from './share.service';
import { RegistationInput } from '../auth/inputs/registration.input';
import { Share } from '@app/common';
@Resolver(() => Share)
export class ShareResolver {
  constructor(private readonly shareService: ShareService) {}

  @Mutation(() => Share)
  async create(@Args('registrationInput') registrationInput: RegistationInput) {
    return await this.shareService.createShare(registrationInput);
  }

  @Query(() => [Share])
  async getShares() {
    return await this.shareService.getShares();
  }
}
