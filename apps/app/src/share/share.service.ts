import { Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';
export class ShareService {
  constructor(
    @Inject('SHARE') private readonly shareServiceClient: ClientProxy,
  ) {}

  public async createShare(defaultInput) {
    const data = await lastValueFrom(
      this.shareServiceClient.send(
        { cmd: 'create-share' },
        {
          ...defaultInput,
        },
      ),
    );
    return data;
  }
  public async getShares() {
    const data = await lastValueFrom(
      this.shareServiceClient.send({ cmd: 'get-shares' }, {}),
    );
    return data;
  }
}
