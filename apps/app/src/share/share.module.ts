import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { RmqModule } from '@app/common';
import { ShareService } from './share.service';
import { ShareResolver } from './share.resolver';
@Module({
  imports: [
    ConfigModule,
    RmqModule.register({
      name: 'SHARE',
    }),
  ],
  providers: [ShareService, ShareResolver],
  controllers: [],
})
export class ShareModule {}
