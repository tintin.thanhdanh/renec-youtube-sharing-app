import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';
@Injectable()
export class AuthService {
  constructor(
    @Inject('AUTH') private readonly authServiceClient: ClientProxy,
  ) {}

  public async register(defaultInput) {
    const data = await lastValueFrom(
      this.authServiceClient.send(
        { cmd: 'register' },
        {
          ...defaultInput,
        },
      ),
    );
    return data;
  }
  public async login(defaultInput) {
    const data = await lastValueFrom(
      this.authServiceClient.send(
        { cmd: 'login' },
        {
          ...defaultInput,
        },
      ),
    );
    return data;
  }
  public async getUser(defaultInput) {
    const data = await lastValueFrom(
      this.authServiceClient.send(
        { cmd: 'get-user' },
        {
          ...defaultInput,
        },
      ),
    );
    return data;
  }
}
