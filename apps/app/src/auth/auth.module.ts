import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { ConfigModule } from '@nestjs/config';
import { AuthResolver } from './auth.resolver';
import { RmqModule } from '@app/common';
@Module({
  imports: [
    ConfigModule,
    RmqModule.register({
      name: 'AUTH',
    }),
  ],
  providers: [AuthService, AuthResolver],
  controllers: [],
})
export class AuthModule {}
