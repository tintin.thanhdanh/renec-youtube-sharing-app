import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';

import { LoginInput } from './inputs/login.input';
import { AuthService } from './auth.service';
import { RegistationInput } from './inputs/registration.input';
import { CurrentUser, JWTUserType, User } from '@app/common';
import { AuthorizationType } from '@app/common/type/authorization.type';
@Resolver(() => User)
export class AuthResolver {
  constructor(private readonly authService: AuthService) {}

  @Mutation(() => AuthorizationType)
  async register(
    @Args('registrationInput') registrationInput: RegistationInput,
  ) {
    return await this.authService.register(registrationInput);
  }

  @Mutation(() => AuthorizationType)
  async login(@Args('loginInput') loginInput: LoginInput) {
    return await this.authService.login(loginInput);
  }

  @Query(() => User)
  async getUser(@CurrentUser() user: JWTUserType) {
    return await this.authService.getUser({ _id: user._id });
  }
}
