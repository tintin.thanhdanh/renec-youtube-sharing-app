import { Module } from '@nestjs/common';
import { ShareService } from './share.service';
import { ConfigModule } from '@nestjs/config';
import { ShareController } from './share.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Share, ShareSchema } from '@app/common';
@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    MongooseModule.forRoot(process.env.DATABASE_URL, {
      connectionName: 'shares',
    }),
    MongooseModule.forFeature(
      [
        {
          name: Share.name,
          schema: ShareSchema,
          collection: 'shares',
        },
      ],
      'shares',
    ),
  ],
  providers: [ShareService],
  controllers: [ShareController],
})
export class ShareModule {}
