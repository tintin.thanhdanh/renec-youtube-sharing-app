import { MessagePattern, Payload } from '@nestjs/microservices';
import { Controller } from '@nestjs/common';
import { ShareService } from './share.service';
import { CreateShareInput } from './inputs/create.input';
@Controller()
export class ShareController {
  constructor(private readonly shareService: ShareService) {}
  @MessagePattern('get-shares')
  async getShares() {
    return await this.shareService.getShares();
  }
  @MessagePattern('create-share')
  async createShare(@Payload() data: CreateShareInput) {
    return await this.shareService.createShare(data);
  }
}
