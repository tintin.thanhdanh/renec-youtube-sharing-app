import { NestFactory } from '@nestjs/core';
import { ShareModule } from './share.module';
import { Logger } from '@nestjs/common';
import { RmqService } from '@app/common';

async function bootstrap() {
  const app = await NestFactory.create(ShareModule);
  const rmqService = app.get<RmqService>(RmqService);
  await app.listen(process.env.PORT || 3003);
  app.connectMicroservice(rmqService.getOptions('SHARE'));
  await app.startAllMicroservices();
  Logger.log(
    'Share microservice is listening at ' + (process.env.PORT || 3003),
  );
}
bootstrap();
