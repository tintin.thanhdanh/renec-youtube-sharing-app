import { Share, ShareDocument } from '@app/common';
import { Injectable } from '@nestjs/common';
import { CreateShareInput } from './inputs/create.input';
import { BaseService } from '@app/common/utils/base-service';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class ShareService extends BaseService {
  constructor(
    @InjectModel(Share.name, 'shares') private ShareModel: Model<ShareDocument>,
  ) {
    super(ShareModel);
  }

  async createShare(createShareInput: CreateShareInput): Promise<Share> {
    const share = await this.createOne(createShareInput);
    return share;
  }

  async getShares(): Promise<Share[]> {
    return await this.find({});
  }
}
