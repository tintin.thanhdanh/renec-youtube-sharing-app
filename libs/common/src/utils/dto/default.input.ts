export class DefaultInput {
  populate?: JSON;
  query?: JSON;
  sort?: JSON;
  search?: string;
}
