import { DefaultInput } from './default.input';

export class PagingInput extends DefaultInput {
  limit?: number;
  page?: number;
}
